import { useCallback, useState } from 'react';
import { Person } from '../../constants/types';

import APIClient from '../../services/APIClient';
import { usePeopleContext } from '../contexts/People.context';

export const usePeople = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const peopleData = usePeopleContext();

  // The following hook (useCallback) may not be neccesary
  // just showing a possible use case
  const execute = useCallback(
    async (size = 20) => {
      setLoading(true);
      try {
        const existingData = JSON.parse(localStorage.getItem('@beon/peopleList'));
        console.log(existingData);
        if (existingData.length) {
          setCurrentPage(currentPage + 1);
          if (peopleData.initialized)
            peopleData.append(existingData, existingData.length);
          else peopleData.initialize(existingData, existingData.length);
          return;
        }

        const { data, totalItems } = await APIClient.getPeopleInfo({
          quantity: size,
          page: currentPage,
        });

        storeData(data);
        setCurrentPage(currentPage + 1);
        if (peopleData.initialized) peopleData.append(data, totalItems);
        else peopleData.initialize(data, totalItems);
      } catch (err) {
        setCurrentPage(0);
        setError(error);
      } finally {
        setLoading(false);
      }
    },
    [peopleData.initialize]
  );

  const storeData = (data: Person[]) => {
    localStorage.setItem('@beon/peopleList', JSON.stringify(data));
  };

  return { ...peopleData, execute, error, loading };
};
